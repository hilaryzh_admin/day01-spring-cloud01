package cn.itcast.feign.clients;

import cn.itcast.feign.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author huan
 * @serial 每天一百行, 致敬未来的自己
 * @Description
 */
@FeignClient("userservice")
public interface UserFeignClient {
    //请求路径：/user/{id}
    @GetMapping("/user/{id}")
    User queryById(@PathVariable("id") Long id);
}
