package cn.itcast.order.service;

import cn.itcast.feign.clients.UserFeignClient;
import cn.itcast.feign.pojo.Order;
import cn.itcast.feign.pojo.User;
import cn.itcast.order.mapper.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    @Autowired
    private OrderMapper orderMapper;

    //@Autowired
    //private RestTemplate restTemplate;

    @Autowired
    private UserFeignClient userFeignClient;

    public Order queryOrderById(Long orderId) {
        // 1.查询订单
        Order order = orderMapper.findById(orderId);
        //2.restTemplate发起请求
        //String url = "http://userservice/user/" + order.getUserId();
        //User user = restTemplate.getForObject(url, User.class);
        User user = userFeignClient.queryById(order.getUserId());
        //3.封装对象
        order.setUser(user);
        // 4.返回
        return order;
    }
}
