package cn.itcast.hotel.pojo;

import lombok.Data;

/**
 * @author huan
 * @serial 每天一百行, 致敬未来的自己
 * @Description
 */
@Data
public class RequestParams {
    /**
     * 搜索关键字
     */
    private String key;
    private Integer page;
    private Integer size;
    /**
     * 排序规则
     */
    private String sortBy;
    /**
     * 品牌值
     */
    private String brand;
    /**
     * 城市
     */
    private String city;
    /**
     * 星级
     */
    private String starName;
    /**
     * 最低价
     */
    private Integer minPrice;
    /**
     * 最高价
     */
    private Integer maxPrice;
    /**
     * 我当前的地理坐标
     */
    private String location;

}
