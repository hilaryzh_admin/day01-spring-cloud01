package cn.itcast.hotel.service;

import cn.itcast.hotel.pojo.Hotel;
import cn.itcast.hotel.pojo.PageResult;
import cn.itcast.hotel.pojo.RequestParams;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

public interface IHotelService extends IService<Hotel> {
    /**
     * 根据关键字搜索酒店信息
     * @param params 请求参数对象,包含用户输入的关键字
     * @return 酒店文档列表
     */
    PageResult search(RequestParams params);

    /**
     * 限定范围聚合查询
     * @param params
     * @return
     */
    Map<String, List<String>> getFilter(RequestParams params);

    /**
     * 搜索框自动补全
     * @param prefix
     * @return
     */
    List<String> getSuggestions(String prefix);

    void deleteById(Long id);

    void insertById(Long id);
}
