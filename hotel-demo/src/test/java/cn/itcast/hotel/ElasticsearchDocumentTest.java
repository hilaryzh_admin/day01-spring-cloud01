package cn.itcast.hotel;

import cn.itcast.hotel.pojo.Hotel;
import cn.itcast.hotel.pojo.HotelDoc;
import cn.itcast.hotel.service.IHotelService;
import com.alibaba.fastjson.JSON;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author huan
 * @serial 每天一百行, 致敬未来的自己
 * @Description
 */
@SpringBootTest
public class ElasticsearchDocumentTest {

    @Autowired
    private IHotelService iHotelService;
    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @BeforeEach
    void setUp() {
        this.restHighLevelClient = new RestHighLevelClient(RestClient.builder(
                HttpHost.create("http://101.35.144.218:9200")
        ));
    }

    @AfterEach
    void tearDown() throws IOException {
        this.restHighLevelClient.close();
    }

    @Test
    void testAddDocument() throws IOException {
        //根据id查询酒店数据
        Hotel hotel = iHotelService.getById(61083L);
        HotelDoc hotelDoc = new HotelDoc(hotel);
        //创建requst对象
        IndexRequest request = new IndexRequest("hotel").id(hotelDoc.getId().toString());
        //准备json文档
        request.source(JSON.toJSONString(hotelDoc), XContentType.JSON);
        //发送请求
        restHighLevelClient.index(request, RequestOptions.DEFAULT);
    }

    /**
     * #查看文档数据
     * GET /hotel/_doc/61083
     */
    @Test
    void testQueryDocument() throws IOException {
        //创建request对象
        GetRequest request = new GetRequest("hotel", "61083");
        //发送请求
        GetResponse response = restHighLevelClient.get(request, RequestOptions.DEFAULT);
        //解析结果
        String json = response.getSourceAsString();
        System.out.println(json);
    }

    /**
     * 修改文档数据
     */
    @Test
    void testUpdateDocument() throws IOException {
        //创建request对象
        UpdateRequest request = new UpdateRequest("hotel", "61083");
        //
        request.doc(
                "age", 18,
                "name", "Rose"
        );
        //发送请求
        restHighLevelClient.update(request, RequestOptions.DEFAULT);
    }

    /**
     * 根据文档id删除
     *
     * @throws IOException
     */
    @Test
    void testDeleteDocument() throws IOException {
        //创建request对象
        DeleteRequest request = new DeleteRequest("hotel", "61083");
        //发送请求
        restHighLevelClient.delete(request, RequestOptions.DEFAULT);
    }

    /**
     * 批量添加文档数据
     *
     * @throws IOException
     */
    @Test
    void testBulkDocument() throws IOException {
        //查询酒店数据
        List<Hotel> hotels = iHotelService.list();
        List<HotelDoc> list = hotels.stream().map(HotelDoc::new).collect(Collectors.toList());
        //创建request对象
        BulkRequest request = new BulkRequest();
        list.forEach(e ->
                request.add(new IndexRequest("hotel").id(e.getId().toString()).source(
                        JSON.toJSONString(e), XContentType.JSON)
                ));
        //发送请求
        restHighLevelClient.bulk(request, RequestOptions.DEFAULT);
    }
}
