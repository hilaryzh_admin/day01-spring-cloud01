package cn.itcast.hotel;

import cn.itcast.hotel.pojo.HotelDoc;
import com.alibaba.fastjson.JSON;
import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.Map;

/**
 * @author huan
 * @serial 每天一百行, 致敬未来的自己
 * @Description
 */
@SpringBootTest
public class HotelQueryApplicationTests {

    @Autowired
    private RestHighLevelClient client;

    @BeforeEach
    void tearUp() {
        this.client = new RestHighLevelClient(RestClient.builder(
                HttpHost.create("http://101.35.144.218:9200")
        ));
    }

    @AfterEach
    void tearDown() throws IOException {
        this.client.close();
    }

    @Test
    void testInit() {
        System.out.println(client);
    }

    /**
     * 全文检索
     *
     * @throws IOException
     */
    @Test
    public void testMathchAll() throws IOException {
        SearchRequest request = new SearchRequest("hotel");
        //组织DSL参数
        request.source().query(QueryBuilders.matchAllQuery());
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //解析响应结果
        parseJsonString(response);
    }

    /**
     * 多字段全文检索
     *
     * @throws IOException
     */
    @Test
    public void testMultiMatchQuery() throws IOException {
        SearchRequest request = new SearchRequest("hotel");
        //组织DSL参数
        request.source().query(QueryBuilders.multiMatchQuery("上海", "name", "business"));
        //发送请求
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //解析响应结果
        parseJsonString(response);
    }

    /**
     * 解析响应结果打印控制台
     *
     * @param response
     */
    public void parseJsonString(SearchResponse response) {
        //解析响应结果
        SearchHits searchHits = response.getHits();
        SearchHit[] hits = searchHits.getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
    }

    /**
     * 精确查询 Term词条精确匹配
     *
     * @throws IOException
     */
    @Test
    public void testTermQuery() throws IOException {
        SearchRequest request = new SearchRequest("hotel");
        //组织DSL参数
        request.source().query(QueryBuilders.termQuery("all", "上海"));
        //发送请求
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //解析响应结果
        parseJsonString(response);
    }

    /**
     * 布尔查询
     *
     * @throws IOException
     */
    @Test
    public void testBooleanQuery() throws IOException {
        SearchRequest request = new SearchRequest("hotel");
        //组织DSL参数
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        boolQuery.should(QueryBuilders.termQuery("city", "北京"));
        boolQuery.must(QueryBuilders.rangeQuery("price").lte(400));
        boolQuery.filter(QueryBuilders.geoDistanceQuery("location").distance("10km").point(31.31, 121.5));
        request.source().query(boolQuery);
        //发送条件
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //解析响应结果
        parseJsonString(response);
    }

    /**
     * 根据评分降序,再根据价格升序,分页查询
     *
     * @throws IOException
     */
    @Test
    public void testPageAndSort() throws IOException {
        SearchRequest request = new SearchRequest("hotel");
        //组织DSL参数,根据评分降序,再根据价格升序
        request.source().query(QueryBuilders.matchAllQuery())
                .sort("score", SortOrder.DESC).sort("price", SortOrder.ASC)
                //from((page - 1) * size).size(5);
                .from(0).size(5);
        //发送条件
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //解析响应结果
        parseJsonString(response);
    }

    /**
     * 全文检索 查询结果高亮显示
     *
     * @throws IOException
     */
    @Test
    void testHighlight() throws IOException {
        SearchRequest request = new SearchRequest("hotel");
        request.source().query(QueryBuilders.matchQuery("all", "如家"));
        //高亮
        request.source().highlighter(new HighlightBuilder().field("name").requireFieldMatch(false));
        //发送请求
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        //解析响应
        SearchHits searchHits = response.getHits();
        SearchHit[] hits = searchHits.getHits();
        for (SearchHit hit : hits) {
            String json = hit.getSourceAsString();
            HotelDoc hotelDoc = JSON.parseObject(json, HotelDoc.class);
            //获取高亮结果
            Map<String, HighlightField> fields = hit.getHighlightFields();
            if (!CollectionUtils.isEmpty(fields)) {
                HighlightField name = fields.get("name");
                if (name != null) {
                    String s = name.getFragments()[0].toString();
                    hotelDoc.setName(s);
                }
            }
            System.out.println("hotelDoc = " + hotelDoc);
        }
    }
}
