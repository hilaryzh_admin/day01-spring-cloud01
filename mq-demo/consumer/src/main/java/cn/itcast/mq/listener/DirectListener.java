package cn.itcast.mq.listener;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author huan
 * @serial 每天一百行, 致敬未来的自己
 * @Description
 */
@Component
public class DirectListener {
    @RabbitListener(bindings = @QueueBinding(//将队列绑定到交换机上
            value = @Queue("direct.queue1"),//创建队列
            exchange = @Exchange(value = "itcast.direct",type = ExchangeTypes.DIRECT),//创建交换机
            key = {"blue", "red"}//路由规则
    ))
    public void linstenerFanoutQueue1(String msg) {
        System.out.println("接收到fanout.queue1队列中的消息了[" + msg + "]" + LocalDateTime.now());
    }
}
