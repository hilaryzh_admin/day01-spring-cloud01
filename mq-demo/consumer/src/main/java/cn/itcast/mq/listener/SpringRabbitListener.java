package cn.itcast.mq.listener;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.time.LocalTime;

/**
 * @author huan
 * @serial 每天一百行, 致敬未来的自己
 * @Description
 */
@Component
public class SpringRabbitListener {

    //@RabbitListener(queues = "simple.queue")
    //public void listenSimpleQueueMessage(String msg)  {
    //    System.out.println("spring 消费者接收到消息：【" + msg + "】");
    //}
    @RabbitListener(queues = "simple.queue")
    public void listenWorkSimpleQueueMessage1(String msg) throws InterruptedException {
        System.out.println("spring 消费者1接收到消息：【" + msg + "】"+ LocalTime.now());
        Thread.sleep(20);
    }
    @RabbitListener(queues = "simple.queue")
    public void listenWorkSimpleQueueMessage2(String msg) throws InterruptedException {
        System.err.println("消费者2..........接收到消息：【" + msg + "】"+ LocalTime.now());
        Thread.sleep(200);
    }
    @RabbitListener(queues = "fanout.queue1")
    public void listenFanoutQueue1(String msg) throws InterruptedException {
        System.err.println("消费者接收到fanout.queue1的消息：【" + msg + "】"+ LocalTime.now());
    }
    @RabbitListener(queues = "fanout.queue2")
    public void listenFanoutQueue2(String msg) throws InterruptedException {
        System.err.println("消费者接收到fanout.queue2的消息：【" + msg + "】"+ LocalTime.now());
    }
}